--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.5
-- Dumped by pg_dump version 9.6.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: education_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE education_detail (
    edu_id integer NOT NULL,
    institute_name text NOT NULL,
    degree text NOT NULL,
    field_of_study text NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    p_id integer
);


ALTER TABLE education_detail OWNER TO postgres;

--
-- Name: education_detail_edu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE education_detail_edu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE education_detail_edu_id_seq OWNER TO postgres;

--
-- Name: education_detail_edu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE education_detail_edu_id_seq OWNED BY education_detail.edu_id;


--
-- Name: experience_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE experience_detail (
    exp_id integer NOT NULL,
    job_title text NOT NULL,
    company text NOT NULL,
    location text NOT NULL,
    start_date date NOT NULL,
    working boolean NOT NULL,
    description text NOT NULL,
    p_id integer NOT NULL
);


ALTER TABLE experience_detail OWNER TO postgres;

--
-- Name: experience_detail_exp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE experience_detail_exp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE experience_detail_exp_id_seq OWNER TO postgres;

--
-- Name: experience_detail_exp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE experience_detail_exp_id_seq OWNED BY experience_detail.exp_id;


--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hibernate_sequence OWNER TO postgres;

--
-- Name: personal_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE personal_detail (
    p_id integer NOT NULL,
    fname text NOT NULL,
    lname text NOT NULL,
    contact_no bigint NOT NULL,
    address text NOT NULL,
    email text NOT NULL,
    dob date NOT NULL,
    nationality text NOT NULL
);


ALTER TABLE personal_detail OWNER TO postgres;

--
-- Name: personal_detail_p_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE personal_detail_p_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE personal_detail_p_id_seq OWNER TO postgres;

--
-- Name: personal_detail_p_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE personal_detail_p_id_seq OWNED BY personal_detail.p_id;


--
-- Name: personal_detail_skills; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE personal_detail_skills (
    personal_detail_p_id integer NOT NULL,
    skills_s_id integer NOT NULL
);


ALTER TABLE personal_detail_skills OWNER TO postgres;

--
-- Name: project_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE project_detail (
    proj_id integer NOT NULL,
    project_name text NOT NULL,
    start_date date NOT NULL,
    end_date date,
    occupation text NOT NULL,
    project_url text NOT NULL,
    description text NOT NULL,
    p_id integer NOT NULL
);


ALTER TABLE project_detail OWNER TO postgres;

--
-- Name: project_detail_proj_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE project_detail_proj_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE project_detail_proj_id_seq OWNER TO postgres;

--
-- Name: project_detail_proj_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE project_detail_proj_id_seq OWNED BY project_detail.proj_id;


--
-- Name: skill_list; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE skill_list (
    s_id integer NOT NULL,
    s_name text NOT NULL
);


ALTER TABLE skill_list OWNER TO postgres;

--
-- Name: skill_list_s_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE skill_list_s_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE skill_list_s_id_seq OWNER TO postgres;

--
-- Name: skill_list_s_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE skill_list_s_id_seq OWNED BY skill_list.s_id;


--
-- Name: user_skill_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE user_skill_detail (
    p_id integer NOT NULL,
    s_id integer NOT NULL
);


ALTER TABLE user_skill_detail OWNER TO postgres;

--
-- Name: education_detail edu_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY education_detail ALTER COLUMN edu_id SET DEFAULT nextval('education_detail_edu_id_seq'::regclass);


--
-- Name: experience_detail exp_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY experience_detail ALTER COLUMN exp_id SET DEFAULT nextval('experience_detail_exp_id_seq'::regclass);


--
-- Name: personal_detail p_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY personal_detail ALTER COLUMN p_id SET DEFAULT nextval('personal_detail_p_id_seq'::regclass);


--
-- Name: project_detail proj_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY project_detail ALTER COLUMN proj_id SET DEFAULT nextval('project_detail_proj_id_seq'::regclass);


--
-- Name: skill_list s_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY skill_list ALTER COLUMN s_id SET DEFAULT nextval('skill_list_s_id_seq'::regclass);


--
-- Data for Name: education_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY education_detail (edu_id, institute_name, degree, field_of_study, start_date, end_date, p_id) FROM stdin;
1	Prime College	Bachelor	Bsc csit	1994-01-18	1999-01-18	1
2	Tribhuvan	Master	CE	1999-02-01	1999-04-03	1
9	Prime College	Bachelor	Bsc csit	1994-01-16	1999-01-16	2
15	trinity College	Bachelor	Bsc csit	1994-01-15	1999-01-15	2
16	NCCS College	Bachelor	Bsc csit	1994-01-15	1999-01-15	3
17	Jubilant College	+2	Science	1994-01-15	1999-01-15	3
7	Jubilant College	+2	BBA	1994-01-15	1999-01-15	1
\.


--
-- Name: education_detail_edu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('education_detail_edu_id_seq', 1, true);


--
-- Data for Name: experience_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY experience_detail (exp_id, job_title, company, location, start_date, working, description, p_id) FROM stdin;
2	Frontend developer	IntoTmT	Hattisar	1997-01-18	t	I am frontend developer in abc company and i have done many projects for that company	2
19	BackEnd developer	IntoTmT	Hattisar	1997-01-17	t	I am backend developer in abc company and i have done many projects for that company	1
20	full mean stack developer	IntoTmT	Hattisar	1997-01-16	t	I am meanstack developer in abc company and i have done many projects for that company	1
\.


--
-- Name: experience_detail_exp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('experience_detail_exp_id_seq', 2, true);


--
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('hibernate_sequence', 22, true);


--
-- Data for Name: personal_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY personal_detail (p_id, fname, lname, contact_no, address, email, dob, nationality) FROM stdin;
1	Anish	Maharjan	9849892055	kathmandu	lookout_froem@lkasjd.com	1999-01-18	nepali
2	Suban	Dhyako	9841122334	Bhaktapur	abc@gmail.com	1992-01-23	nepali
3	Ram	Shrest	9873821818	ktm	abc@gaks.com	1992-02-01	indian
\.


--
-- Name: personal_detail_p_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('personal_detail_p_id_seq', 2, true);


--
-- Data for Name: personal_detail_skills; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY personal_detail_skills (personal_detail_p_id, skills_s_id) FROM stdin;
\.


--
-- Data for Name: project_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY project_detail (proj_id, project_name, start_date, end_date, occupation, project_url, description, p_id) FROM stdin;
1	CV evaluation and manipulation	1999-01-16	2005-01-16	front end	http://www.abc.com	THis is project that evaluates the cv of candidates	2
21	Autologin	1999-01-15	2005-01-15	front end	http://www.abc.com	THis is project that login	1
\.


--
-- Name: project_detail_proj_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('project_detail_proj_id_seq', 2, true);


--
-- Data for Name: skill_list; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY skill_list (s_id, s_name) FROM stdin;
1	java
2	bootstrap
3	js
4	html
5	c++
6	c
\.


--
-- Name: skill_list_s_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('skill_list_s_id_seq', 6, true);


--
-- Data for Name: user_skill_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY user_skill_detail (p_id, s_id) FROM stdin;
2	1
2	2
2	3
1	1
1	2
1	3
1	4
1	5
\.


--
-- Name: education_detail education_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY education_detail
    ADD CONSTRAINT education_detail_pkey PRIMARY KEY (edu_id);


--
-- Name: experience_detail experience_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY experience_detail
    ADD CONSTRAINT experience_detail_pkey PRIMARY KEY (exp_id);


--
-- Name: personal_detail personal_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY personal_detail
    ADD CONSTRAINT personal_detail_pkey PRIMARY KEY (p_id);


--
-- Name: project_detail project_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY project_detail
    ADD CONSTRAINT project_detail_pkey PRIMARY KEY (proj_id);


--
-- Name: skill_list skill_list_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY skill_list
    ADD CONSTRAINT skill_list_pkey PRIMARY KEY (s_id);


--
-- Name: user_skill_detail user_skill_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_skill_detail
    ADD CONSTRAINT user_skill_detail_pkey PRIMARY KEY (p_id, s_id);


--
-- Name: education_detail education_detail_p_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY education_detail
    ADD CONSTRAINT education_detail_p_id_fkey FOREIGN KEY (p_id) REFERENCES personal_detail(p_id);


--
-- Name: experience_detail experience_detail_p_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY experience_detail
    ADD CONSTRAINT experience_detail_p_id_fkey FOREIGN KEY (p_id) REFERENCES personal_detail(p_id);


--
-- Name: education_detail fk2o6rb09ueuu7ws692lcqa7o4e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY education_detail
    ADD CONSTRAINT fk2o6rb09ueuu7ws692lcqa7o4e FOREIGN KEY (p_id) REFERENCES personal_detail(p_id);


--
-- Name: experience_detail fk7q6s3hvsq0d306bxipflwdhmb; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY experience_detail
    ADD CONSTRAINT fk7q6s3hvsq0d306bxipflwdhmb FOREIGN KEY (p_id) REFERENCES personal_detail(p_id);


--
-- Name: user_skill_detail fkd75sm16sqpj97v0u4oetmam37; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_skill_detail
    ADD CONSTRAINT fkd75sm16sqpj97v0u4oetmam37 FOREIGN KEY (p_id) REFERENCES personal_detail(p_id);


--
-- Name: project_detail fkmv8pstfeuv6bkpcd6bk4i9qi3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY project_detail
    ADD CONSTRAINT fkmv8pstfeuv6bkpcd6bk4i9qi3 FOREIGN KEY (p_id) REFERENCES personal_detail(p_id);


--
-- Name: user_skill_detail fkqawu05svtecb3vrfa7nxrs017; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_skill_detail
    ADD CONSTRAINT fkqawu05svtecb3vrfa7nxrs017 FOREIGN KEY (s_id) REFERENCES skill_list(s_id);


--
-- Name: project_detail project_detail_p_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY project_detail
    ADD CONSTRAINT project_detail_p_id_fkey FOREIGN KEY (p_id) REFERENCES personal_detail(p_id);


--
-- Name: user_skill_detail user_skill_detail_p_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_skill_detail
    ADD CONSTRAINT user_skill_detail_p_id_fkey FOREIGN KEY (p_id) REFERENCES personal_detail(p_id);


--
-- Name: user_skill_detail user_skill_detail_s_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_skill_detail
    ADD CONSTRAINT user_skill_detail_s_id_fkey FOREIGN KEY (s_id) REFERENCES skill_list(s_id);


--
-- PostgreSQL database dump complete
--

