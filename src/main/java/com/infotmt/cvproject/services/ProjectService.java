package com.infotmt.cvproject.services;

import java.util.Collection;

import com.infotmt.cvproject.model.ProjectDetail;

public interface ProjectService {
	
	Collection<ProjectDetail> findAll();
	
	ProjectDetail findOne(int id);
	
	ProjectDetail save(ProjectDetail projectDetail);
	
	ProjectDetail update(ProjectDetail projectDetail);
	
	void delete(int id);
}
