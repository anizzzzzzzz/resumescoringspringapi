package com.infotmt.cvproject.services.servicesimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infotmt.cvproject.model.SkillList;
import com.infotmt.cvproject.repository.SkillRepository;
import com.infotmt.cvproject.services.SkillService;

@Service
public class SkillServiceImpl implements SkillService {
	
	@Autowired
	SkillRepository skillRepository;

	@Override
	public List<SkillList> findAll() {
		return skillRepository.findAll();
	}

	@Override
	public SkillList findOne(int id) {
		return skillRepository.findOne(id);
	}

	@Override
	public SkillList save(SkillList skillList) {
		return skillRepository.save(skillList);
	}

	@Override
	public SkillList update(SkillList skillList) {
		return skillRepository.save(skillList);
	}

	@Override
	public void delete(int id) {
		skillRepository.delete(id);
	}

}
