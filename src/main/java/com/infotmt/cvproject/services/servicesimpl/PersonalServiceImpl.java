package com.infotmt.cvproject.services.servicesimpl;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.infotmt.cvproject.model.EducationDetail;
import com.infotmt.cvproject.model.ExperienceDetail;
import com.infotmt.cvproject.model.PersonalDetail;
import com.infotmt.cvproject.model.ProjectDetail;
import com.infotmt.cvproject.repository.EducationRepository;
import com.infotmt.cvproject.repository.ExperienceRepository;
import com.infotmt.cvproject.repository.PersonalRepository;
import com.infotmt.cvproject.repository.ProjectRepository;
import com.infotmt.cvproject.services.PersonalService;
import com.infotmt.cvproject.specification.UserSpecificationsBuilder;

@Service
public class PersonalServiceImpl implements PersonalService {
	
	@Autowired
	PersonalRepository personalRepository;
	
	@Autowired
	EducationRepository educationRepository;
	
	@Autowired
	ExperienceRepository experienceRepository;
	
	@Autowired
	ProjectRepository projectRepository;

	@Override
	public Page<PersonalDetail> findAll(Pageable pageable) {
		return personalRepository.findAll(pageable);
	}

	@Override
	public PersonalDetail findOne(int id) {
		return personalRepository.findOne(id);
	}

	@Override
	public PersonalDetail save(PersonalDetail personalDetail) {
		return personalRepository.save(personalDetail);
	}

	@Override
	public PersonalDetail update(PersonalDetail personalDetail) {
		return personalRepository.save(personalDetail);
	}

	@Override
	public void delete(int id) {
		personalRepository.delete(id);		
	}
	

	@Override
	public Specification<PersonalDetail> getSpecification(String search) {
		UserSpecificationsBuilder builder = new UserSpecificationsBuilder();
        Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),",Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(search + ",");
        while (matcher.find()) {
            builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
        }
         
        Specification<PersonalDetail> spec = builder.build();
        
        return spec;
	}

	
	//----------------------------Education------------------------------------

	@Override
	public EducationDetail findByEduIdAndPersonalDetailPId(int eduid, int pid) {
		return  educationRepository.findByEduIdAndPersonalDetailPId(eduid, pid);
	}

	@Override
	public EducationDetail saveEducation(EducationDetail educationDetail) {
		return educationRepository.save(educationDetail);
	}

	@Override
	public EducationDetail updateEducation(EducationDetail educationDetail) {
		return educationRepository.save(educationDetail);
	}
	
	@Override
	public void deleteEducation(int eduid) {
		educationRepository.delete(eduid);
	}

	//------------------------Experience-------------------------------
	@Override
	public ExperienceDetail findByExpIdAndPersonalDetailPId(int expid, int pid) {
		return  experienceRepository.findByExpIdAndPersonalDetailPId(expid, pid);
	}

	@Override
	public ExperienceDetail saveExperience(ExperienceDetail experienceDetail) {
		return experienceRepository.save(experienceDetail);
	}

	@Override
	public ExperienceDetail updateExperience(ExperienceDetail experienceDetail) {
		return experienceRepository.save(experienceDetail);
	}

	@Override
	public void deleteExperience(int eduid) {
		experienceRepository.delete(eduid);
		
	}
	
	
	//-----------------------Project-------------------------------

	@Override
	public ProjectDetail findByProjIdAndPersonalDetailPId(int projid, int pid) {
		return  projectRepository.findByProjIdAndPersonalDetailPId(projid, pid);
	}

	@Override
	public ProjectDetail saveProject(ProjectDetail projectDetail) {
		return projectRepository.save(projectDetail);
	}

	@Override
	public ProjectDetail updateProject(ProjectDetail projectDetail) {
		return projectRepository.save(projectDetail);
	}

	@Override
	public void deleteProkect(int projid) {
		projectRepository.delete(projid);
		
	}

}
