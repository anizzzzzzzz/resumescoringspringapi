package com.infotmt.cvproject.services.servicesimpl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infotmt.cvproject.model.ProjectDetail;
import com.infotmt.cvproject.repository.ProjectRepository;
import com.infotmt.cvproject.services.ProjectService;

@Service
public class ProjectServiceImpl implements ProjectService{
	
	@Autowired
	ProjectRepository projectRepository;

	@Override
	public Collection<ProjectDetail> findAll() {
		return projectRepository.findAll();
	}
	
	@Override
	public ProjectDetail findOne(int id) {
		return projectRepository.findOne(id);
	}

	@Override
	public ProjectDetail save(ProjectDetail projectDetail) {
		return projectRepository.save(projectDetail);
	}

	@Override
	public ProjectDetail update(ProjectDetail projectDetail) {
		return projectRepository.save(projectDetail);
	}

	@Override
	public void delete(int id) {
		projectRepository.delete(id);
		
	}

}
