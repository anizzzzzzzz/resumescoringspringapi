package com.infotmt.cvproject.services.servicesimpl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.infotmt.cvproject.model.EducationDetail;
import com.infotmt.cvproject.repository.EducationRepository;
import com.infotmt.cvproject.services.EducationService;

@Service
public class EducationServiceImpl implements EducationService {
	
	@Autowired
	EducationRepository educationRepository;

	@Override
	public Collection<EducationDetail> findAll() {
		return educationRepository.findAll();
	}

	@Override
	public EducationDetail findOne(int id) {
		return educationRepository.findOne(id);
	}

	@Override
	public EducationDetail save(EducationDetail educationDetail) {		
		return educationRepository.save(educationDetail);
	}

	@Override
	public EducationDetail update(EducationDetail educationDetail) {
		return educationRepository.save(educationDetail);
	}

	@Override
	public void delete(int id) {
		educationRepository.delete(id);
	}

	@Override
	public Page<EducationDetail> listAllByPage(Pageable pageable) {
		return educationRepository.findAll(pageable);
	}

}
