package com.infotmt.cvproject.services.servicesimpl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infotmt.cvproject.model.ExperienceDetail;
import com.infotmt.cvproject.repository.ExperienceRepository;
import com.infotmt.cvproject.services.ExperienceService;

@Service
public class ExperienceServiceImpl implements ExperienceService{
	
	@Autowired
	ExperienceRepository experienceRepository;

	@Override
	public Collection<ExperienceDetail> findAll() {
		return experienceRepository.findAll();
	}

	@Override
	public ExperienceDetail findOne(int id) {
		return experienceRepository.findOne(id);
	}

	@Override
	public ExperienceDetail save(ExperienceDetail experienceDetail) {
		return experienceRepository.save(experienceDetail);
	}

	@Override
	public ExperienceDetail update(ExperienceDetail experienceDetail) {
		return experienceRepository.save(experienceDetail);
	}

	@Override
	public void delete(int id) {
		experienceRepository.delete(id);
	}

}
