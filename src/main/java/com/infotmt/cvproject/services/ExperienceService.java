package com.infotmt.cvproject.services;

import java.util.Collection;

import com.infotmt.cvproject.model.ExperienceDetail;

public interface ExperienceService {
	
	Collection<ExperienceDetail> findAll();
	
	ExperienceDetail findOne(int id);
	
	ExperienceDetail save(ExperienceDetail experienceDetail);
	
	ExperienceDetail update(ExperienceDetail experienceDetail);
	
	void delete(int id);

}
