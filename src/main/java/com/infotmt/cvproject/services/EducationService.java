package com.infotmt.cvproject.services;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.infotmt.cvproject.model.EducationDetail;

public interface EducationService {
	
	Collection<EducationDetail> findAll();
	
	EducationDetail findOne(int id);
	
	EducationDetail save(EducationDetail educationDetail);
	
	EducationDetail update(EducationDetail educationDetail);
	
	void delete(int id);
	
	Page<EducationDetail> listAllByPage(Pageable pageable);
	
}
