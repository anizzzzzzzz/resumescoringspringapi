package com.infotmt.cvproject.services;

import java.util.List;

import com.infotmt.cvproject.model.SkillList;

public interface SkillService {
	
	List<SkillList> findAll();
	
	SkillList findOne(int id);
	
	SkillList save(SkillList skillList);
	
	SkillList update(SkillList skillList);
	
	void delete(int id);

}
