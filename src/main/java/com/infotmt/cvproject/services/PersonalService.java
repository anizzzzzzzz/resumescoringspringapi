package com.infotmt.cvproject.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import com.infotmt.cvproject.model.EducationDetail;
import com.infotmt.cvproject.model.ExperienceDetail;
import com.infotmt.cvproject.model.PersonalDetail;
import com.infotmt.cvproject.model.ProjectDetail;

public interface PersonalService {
	
	Page<PersonalDetail> findAll(Pageable pageable);
	
	PersonalDetail findOne(int id);
	
	PersonalDetail save(PersonalDetail personalDetail);
	
	PersonalDetail update(PersonalDetail personalDetail);
	
	void delete(int id);
	
	Specification<PersonalDetail> getSpecification(String search);
	
	//-----------------Education---------------------------
	
	EducationDetail findByEduIdAndPersonalDetailPId(int eduid,int pid);
	
	EducationDetail saveEducation(EducationDetail educationDetail);

	EducationDetail updateEducation(EducationDetail educationDetail);
	
	void deleteEducation(int eduid);
	
	//----------------Experience---------------------------------
	
	ExperienceDetail findByExpIdAndPersonalDetailPId(int expid,int pid);
	
	ExperienceDetail saveExperience(ExperienceDetail experienceDetail);

	ExperienceDetail updateExperience(ExperienceDetail experienceDetail);
	
	void deleteExperience(int expid);
	
	
	//------------------------Project------------------------------
	
	ProjectDetail findByProjIdAndPersonalDetailPId(int projid,int pid);
	
	ProjectDetail saveProject(ProjectDetail projectDetail);

	ProjectDetail updateProject(ProjectDetail projectDetail);
	
	void deleteProkect(int projid);

}
