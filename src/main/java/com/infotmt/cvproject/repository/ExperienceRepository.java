package com.infotmt.cvproject.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import com.infotmt.cvproject.model.ExperienceDetail;

public interface ExperienceRepository extends JpaRepository<ExperienceDetail, Integer> {
	
	Collection<ExperienceDetail> findByPersonalDetailPId(int pid);
	
	ExperienceDetail findByExpIdAndPersonalDetailPId(int expid, int pid);

}
