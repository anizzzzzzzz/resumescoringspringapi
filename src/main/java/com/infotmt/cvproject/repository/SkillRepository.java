package com.infotmt.cvproject.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.infotmt.cvproject.model.SkillList;

public interface SkillRepository extends JpaRepository<SkillList, Integer> {
		
}
