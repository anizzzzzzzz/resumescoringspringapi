package com.infotmt.cvproject.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import com.infotmt.cvproject.model.ProjectDetail;

public interface ProjectRepository extends JpaRepository<ProjectDetail, Integer>{
	
	Collection<ProjectDetail> findByPersonalDetailPId(int pid);

	ProjectDetail findByProjIdAndPersonalDetailPId(int projid, int pid);
}
