package com.infotmt.cvproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.infotmt.cvproject.model.PersonalDetail;

public interface PersonalRepository extends JpaRepository<PersonalDetail, Integer>,JpaSpecificationExecutor<PersonalDetail>  {
	
	
}
