package com.infotmt.cvproject.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import com.infotmt.cvproject.model.EducationDetail;

public interface EducationRepository extends JpaRepository<EducationDetail,Integer>{
	
	Collection<EducationDetail> findByPersonalDetailPId(int pid);
	
	EducationDetail findByEduIdAndPersonalDetailPId(int eduid, int pid);
	
	void delete(int id);
	
	
}
