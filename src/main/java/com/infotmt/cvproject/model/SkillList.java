package com.infotmt.cvproject.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class SkillList {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int sId;
	private String sName;
	
}
