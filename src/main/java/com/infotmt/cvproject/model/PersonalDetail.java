package com.infotmt.cvproject.model;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.NoArgsConstructor;



@Entity
@Data
@NoArgsConstructor
public class PersonalDetail {
	

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int pId;
	
	@Column(name="fname")
	private String fname1;
	private String lname;
	private long contactNo;
	private String address;
	private String email;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date dob;
	private String nationality;	
	
	
	 @OneToMany(mappedBy = "personalDetail",fetch = FetchType.EAGER)
	 private Collection<EducationDetail> educationDetails;
	 
	 @OneToMany(mappedBy="personalDetail")
	 private Collection<ExperienceDetail> experienceDetail;
	 
	 @OneToMany(mappedBy="personalDetail")
	 private Collection<ProjectDetail> projectDetail;
	 
	 @ManyToMany
	 @JoinTable(name="user_skill_detail",
			   joinColumns = @JoinColumn(name = "p_id"),
		        inverseJoinColumns = @JoinColumn(name = "s_id"))
	 private List<SkillList> skills;

	public PersonalDetail(int pId, String fname1, String lname, long contactNo, String address, String email, Date dob,String nationality) {
		this.pId = pId;
		this.fname1 = fname1;
		this.lname = lname;
		this.contactNo = contactNo;
		this.address = address;
		this.email = email;
		this.dob = dob;
		this.nationality = nationality;
	}

	
	 
	 
}
