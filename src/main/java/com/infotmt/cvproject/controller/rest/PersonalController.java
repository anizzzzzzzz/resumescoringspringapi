package com.infotmt.cvproject.controller.rest;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.infotmt.cvproject.model.EducationDetail;
import com.infotmt.cvproject.model.ExperienceDetail;
import com.infotmt.cvproject.model.PersonalDetail;
import com.infotmt.cvproject.model.ProjectDetail;
import com.infotmt.cvproject.repository.PersonalRepository;
import com.infotmt.cvproject.services.EducationService;
import com.infotmt.cvproject.services.PersonalService;

@RestController
@RequestMapping("/personal")
public class PersonalController {
	
	@Autowired
	PersonalService personalService;
	
	@Autowired
	PersonalRepository personalRepository;
	
	@Autowired
	EducationService educationService;

//	
//	@GetMapping
//	public List<PersonalDetail> findAll()
//	{
//		return personalService.findAll();
//	}
	
	@GetMapping("/{id}")
	public PersonalDetail findOne(@PathVariable int id)
	{
		return personalService.findOne(id);
	}
	
	
	@PostMapping
	public PersonalDetail save(@RequestBody PersonalDetail educationDetail)
	{
		return personalService.save(educationDetail);
	}
	
	@PutMapping
	public PersonalDetail update(@RequestBody PersonalDetail personalDetail)
	{
		return personalService.save(personalDetail);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable int id)
	{
		personalService.delete(id);
	}
	
	@GetMapping
	@ResponseBody
    public Page<PersonalDetail> getAll(@RequestParam(value = "", required=false) String search,Pageable pageable) {      
		
        Specification<PersonalDetail> spec = personalService.getSpecification(search);
        
        return personalRepository.findAll(spec,pageable);
    }
	
	//-------------------------Education----------------------------------
	
	@GetMapping("/{id}/education")											
	public Collection<EducationDetail> findAllEducation(@PathVariable int id){
		return personalService.findOne(id).getEducationDetails();
	}
	
	@GetMapping("/{id}/education/{eduid}")
	public EducationDetail findOneEducation(@PathVariable int id, @PathVariable int eduid)
	{
		return personalService.findByEduIdAndPersonalDetailPId(eduid, id);
	}
	
	@PostMapping("/{personalid}/education")
	public EducationDetail saveEducation(@RequestBody EducationDetail educationDetail,@PathVariable int personalid)
	{
		educationDetail.setPersonalDetail(new PersonalDetail(personalid, "", "", 0, "", "", null, ""));
		return personalService.saveEducation(educationDetail);
	}
	
	@PutMapping("/{personalid}/education")
	public EducationDetail updateEducation(@RequestBody EducationDetail educationDetail,@PathVariable int personalid)
	{
		educationDetail.setPersonalDetail(new PersonalDetail(personalid, "", "", 0, "", "", null, ""));
		return personalService.updateEducation(educationDetail);
		
	}
	
	/*@DeleteMapping("/{personalid}/education/{eduId}")
	public void deleteEdu(@PathVariable int eduId)
	{
		
	}*/
	
	//-------------------------Experience---------------------------------------
	@GetMapping("/{id}/experience")											
	public Collection<ExperienceDetail> findAllExperience(@PathVariable int id){
		return personalService.findOne(id).getExperienceDetail();
	}
	
	@GetMapping("/{id}/experience/{expid}")
	public ExperienceDetail findOneExperience(@PathVariable int id, @PathVariable int expid)
	{
		return personalService.findByExpIdAndPersonalDetailPId(expid, id);
	}
	
	@PostMapping("/{personalid}/experience")
	public ExperienceDetail saveExperience(@RequestBody ExperienceDetail experienceDetail,@PathVariable int personalid)
	{
		experienceDetail.setPersonalDetail(new PersonalDetail(personalid, "", "", 0, "", "", null, ""));
		return personalService.saveExperience(experienceDetail);
	}
	
	@PutMapping("/{personalid}/experience")
	public ExperienceDetail updateExperience(@RequestBody ExperienceDetail experienceDetail,@PathVariable int personalid)
	{
		experienceDetail.setPersonalDetail(new PersonalDetail(personalid, "", "", 0, "", "", null, ""));
		return personalService.updateExperience(experienceDetail);
	}
	
	/*@DeleteMapping("/{personalid}/education/{eduId}")
	public void deleteEdu(@PathVariable int eduId)
	{
		
	}*/
	
	
	//-------------------------Project---------------------------------------
		@GetMapping("/{id}/project")											
		public Collection<ProjectDetail> findAllProject(@PathVariable int id){
			return personalService.findOne(id).getProjectDetail();
		}
		
		@GetMapping("/{id}/project/{projid}")
		public ProjectDetail findOneProject(@PathVariable int id, @PathVariable int projid)
		{
			return personalService.findByProjIdAndPersonalDetailPId(projid, id);
		}
		
		@PostMapping("/{personalid}/project")
		public ProjectDetail saveExperience(@RequestBody ProjectDetail projectDetail,@PathVariable int personalid)
		{
			projectDetail.setPersonalDetail(new PersonalDetail(personalid, "", "", 0, "", "", null, ""));
			return personalService.saveProject(projectDetail);
		}
		
		@PutMapping("/{personalid}/project")
		public ProjectDetail updateProject(@RequestBody ProjectDetail projectDetail,@PathVariable int personalid)
		{
			projectDetail.setPersonalDetail(new PersonalDetail(personalid, "", "", 0, "", "", null, ""));
			return personalService.updateProject(projectDetail);
		}
		
		/*@DeleteMapping("/{personalid}/education/{eduId}")
		public void deleteEdu(@PathVariable int eduId)
		{
			
		}*/
	
	
}
