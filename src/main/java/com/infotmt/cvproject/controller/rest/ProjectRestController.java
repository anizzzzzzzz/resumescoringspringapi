package com.infotmt.cvproject.controller.rest;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.infotmt.cvproject.model.ProjectDetail;
import com.infotmt.cvproject.services.ProjectService;

@RestController
@RequestMapping("/project")
public class ProjectRestController {
	
	@Autowired
	ProjectService projectService;
	
	
	@GetMapping
	public Collection<ProjectDetail> findAll()
	{
		return projectService.findAll();
	}
	
	@GetMapping("/{proid}")
	public ProjectDetail findOne(@PathVariable int proid)
	{
		return projectService.findOne(proid);
	}	
	
	
	@DeleteMapping("/{proid}")
	public void delete(@PathVariable int proid)
	{
		projectService.delete(proid);
	}

}
