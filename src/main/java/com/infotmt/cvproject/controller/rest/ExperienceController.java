package com.infotmt.cvproject.controller.rest;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.infotmt.cvproject.model.ExperienceDetail;
import com.infotmt.cvproject.services.ExperienceService;

@RestController
@RequestMapping("/experience")
public class ExperienceController {
	
	@Autowired
	ExperienceService experienceService;

	
	@GetMapping
	public Collection<ExperienceDetail> findAll()
	{
		return experienceService.findAll();
	}
	
	@GetMapping("/{expid}")
	public ExperienceDetail findOne(@PathVariable int expid)
	{
		return experienceService.findOne(expid);
	}	
	
	
	@DeleteMapping("/{expId}")
	public void delete(@PathVariable int expId)
	{
		experienceService.delete(expId);
	}

}
