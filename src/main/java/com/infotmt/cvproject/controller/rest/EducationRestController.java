package com.infotmt.cvproject.controller.rest;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.infotmt.cvproject.model.EducationDetail;
import com.infotmt.cvproject.services.EducationService;


@RestController
@RequestMapping("/education")
public class EducationRestController {
	
	@Autowired
	EducationService educationService;
	
	
	@GetMapping
	public Page<EducationDetail> findAll(Pageable pageable,HttpServletResponse response)
	{
		
		return educationService.listAllByPage(pageable);
		
	}
	
	@GetMapping("/{eduid}")
	public EducationDetail findOne(@PathVariable int eduid)
	{
		return educationService.findOne(eduid);
	}	
	
	
	@DeleteMapping("/{eduId}")
	public void delete(@PathVariable int eduId)
	{
		educationService.delete(eduId);
	}

}
