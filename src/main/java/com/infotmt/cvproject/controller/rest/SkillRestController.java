package com.infotmt.cvproject.controller.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.infotmt.cvproject.model.SkillList;
import com.infotmt.cvproject.services.SkillService;

@RestController
@RequestMapping("/skill")
public class SkillRestController {
	
	@Autowired
	SkillService skillService;
	
	@GetMapping
	public List<SkillList> findAll(){
		return skillService.findAll();
	}
	
	@GetMapping("/{id}")
	public SkillList findOne(@PathVariable int id) {
		return skillService.findOne(id);
	}
	
	@PostMapping
	public SkillList save(@RequestBody SkillList skillList) {
		return skillService.save(skillList);
	}
	
	@PutMapping
	public SkillList update(@RequestBody SkillList skillList) {
		return skillService.save(skillList);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable int id) {
		skillService.delete(id);
	}

}
